import { combineReducers, configureStore } from "@reduxjs/toolkit";
import commonSlice from "@/common/common.slice";
import authSlice from "@/pages/Login/auth.slice";
import productSlice from "@/pages/home/product.slice";

const rootReducers = combineReducers({
  common: commonSlice.reducer,
  auth: authSlice.reducer,
  product: productSlice.reducer,
});

export const rootStore = configureStore({
  reducer: rootReducers,
});

export const { dispatch } = rootStore;
export type RootState = ReturnType<typeof rootStore.getState>;
