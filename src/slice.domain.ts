import { RootState, rootStore } from "./slice.index";

export type GetState = () => RootState;
export type AppDispatch = typeof rootStore.dispatch;