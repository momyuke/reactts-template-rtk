import { Navigate, Route, Routes } from "react-router-dom";
import Home from "./pages/home";
import Login from "./pages/Login";
import Cart from "./pages/cart";

export const RoutesApp = () => {
  return (
    <div className="bg-gr">
      <Routes>
        <Route path="/" element={<Navigate to={"/home"} />}></Route>
        <Route element={<Home />} path="/home" index={true}></Route>

        <Route path="/login" element={<Login />} index={true}></Route>
        <Route path="/cart" element={<Cart />} index={true}></Route>
        {/* <Route element={<ContactPage />} path="/contact" index={true}></Route> */}
      </Routes>
      {/* <Footer /> */}
    </div>
  );
};
