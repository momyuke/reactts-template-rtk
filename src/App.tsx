import { BrowserRouter } from "react-router-dom";
import "./App.css";
import { CommonDialog } from "@/common/component/dialog/dialog.common";
import { CommonDialogId } from "@/common/common.domain";
import { RoutesApp } from "./routes.index";

function App() {
  return (
    <>
      <BrowserRouter>
        <RoutesApp />
      </BrowserRouter>
      <CommonDialog dialogId={CommonDialogId.GENERAL_ERROR}></CommonDialog>
    </>
  );
}

export default App;
