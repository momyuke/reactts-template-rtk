import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import enJson from "./en.json";
import idJson from "./id.json";

i18next.use(initReactI18next).init({
  resources: {
    en: { ...enJson },
    id: { ...idJson },
  },
  lng: "en",
});
