import { useDispatch } from "react-redux";
import { AppDispatch } from '../../slice.domain';

export const useAppDispatch = useDispatch.withTypes<AppDispatch>()