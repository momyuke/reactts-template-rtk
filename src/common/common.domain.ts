/* eslint-disable @typescript-eslint/no-explicit-any */
export const CommonDialogId = {
  GENERAL_ERROR: "GENERAL_ERROR",
  LOGIN: "LOGIN",
};

export interface IDialogData {
  message: string;
  title: string;
  key: string;
}

export interface CommonInitialState {
  isLayerOpen: any;
  isLoading: any;
  dialogData: any;
}

export interface GeneralResponse<T> {
  message: string;
  data?: T;
  statusCode: number;
}
