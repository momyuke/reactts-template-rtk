import { CaseReducer, PayloadAction, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../slice.index";
import { CommonInitialState, IDialogData } from "./common.domain";
export const commonSelector = {
  isLayerOpen: (state: RootState) => state.common.isLayerOpen,
  isLoading: (state: RootState) => state.common.isLoading,
  getDialogData: (state: RootState) => state.common.dialogData,
};

const initialState: CommonInitialState = {
  isLayerOpen: {},
  isLoading: {},
  dialogData: {},
};

const updateData: CaseReducer<
  CommonInitialState,
  PayloadAction<IDialogData>
> = (state, { payload }) => {
  console.log(state);
  return {
    ...state,
    dialogData: {
      [payload.key]: {
        ...payload,
      },
    },
  };
};

const openLayer: CaseReducer<CommonInitialState, PayloadAction<string>> = (
  state,
  { payload }
) => {
  console.log(state);
  return {
    ...state,
    isLayerOpen: {
      ...state.isLayerOpen,
      [payload]: true,
    },
  };
};

const closeLayer: CaseReducer<CommonInitialState, PayloadAction<string>> = (
  state,
  { payload }
) => {
  return {
    ...state,
    isLayerOpen: {
      ...state.isLayerOpen,
      [payload]: false,
    },
  };
};

const openLoading: CaseReducer<CommonInitialState, PayloadAction<string>> = (
  state,
  { payload }
) => {
  console.log(state);
  return {
    ...state,
    isLoading: {
      [payload]: true,
    },
  };
};

const closeLoading: CaseReducer<CommonInitialState, PayloadAction<string>> = (
  state,
  { payload }
) => {
  return {
    ...state,
    isLoading: {
      [payload]: false,
    },
  };
};

const commonSlice = createSlice({
  name: "common",
  initialState,
  reducers: {
    openLayer,
    closeLayer,
    openLoading,
    closeLoading,
    updateData,
  },
});

export const commonActions = commonSlice.actions;
export default commonSlice;
