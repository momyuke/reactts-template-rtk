import { CSSProperties, ReactNode, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { commonSelector } from "../common.slice";
import "./styles/layer.modules.css";

interface LayerProps {
  layerId: string;
  children: ReactNode;
  styleClass?: string;
}

/**
 * @description Component that will handle to overlap the entire screen (Usually for dialog or model)
 * @param {LayerProps} props
 * @param {string} props.layerId - ID to identify state to decide layer is being opened or not
 * @returns {ReactNode}
 */
export const Layer = (props: LayerProps) => {
  const isLayerOpen = useSelector(commonSelector.isLayerOpen)[props.layerId];
  const [isFirstMount, setIsFirstMount] = useState<boolean>(true);

  const layerDisplayStyle = useMemo((): CSSProperties => {
    if (isLayerOpen) {
      return {
        display: "block",
      };
    }

    if (isFirstMount) {
      setIsFirstMount(false);
      return { display: "none" };
    }

    return {};
  }, [isLayerOpen]);

  const layerAnimationStyle = useMemo((): CSSProperties => {
    if (!isLayerOpen) {
      return {
        animation: "onClose .3s ease-in forwards",
      };
    }
    return {
      animation: "onOpen .3s ease-in forwards",
    };
  }, [isLayerOpen]);

  return (
    <>
      <div
        style={{ ...layerDisplayStyle, ...layerAnimationStyle }}
        className={`layer ${props?.styleClass}`}
      >
        {props.children}
      </div>
    </>
  );
};
