import { Layer } from "../layer.common";
import style from "./loading.module.css";

interface ILoadingProps {
  loadingId: string;
}
export const Loading = ({ loadingId }: ILoadingProps) => {
  return (
    <Layer layerId={loadingId}>
      <main className={`${style.loaderWrapper}`}>
        <div className={`${style.loader}`}></div>
      </main>
    </Layer>
  );
};
