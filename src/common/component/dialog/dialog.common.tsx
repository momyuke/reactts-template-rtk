import { MouseEventHandler, ReactNode, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IDialogData } from "@/common/common.domain";
import { commonActions, commonSelector } from "@/common/common.slice";
import { Layer } from "../layer.common";
import "./dialog.modules.css";

interface CommonDialogProps {
  dialogId: string;
  header?: CommonDialogHeaderProps;
  body?: CommonDialogBodyProps;
  children?: ReactNode;
}

interface CommonDialogHeaderProps {
  title: string;
}

interface CommonDialogBodyProps {
  children: ReactNode;
}

interface CommonDialogFooterProps {
  onClickCancel?: MouseEventHandler<HTMLButtonElement>;
  onClickSuccess?: MouseEventHandler<HTMLButtonElement>;
  titleCancel: string;
  titleSuccess: string;
}

const DialogHeader = (props: CommonDialogHeaderProps) => {
  const { title } = props;
  return (
    <div className="dialog-header f-column absolute-center">
      <h3>{title}</h3>
    </div>
  );
};

const DialogBody = (props: CommonDialogBodyProps) => {
  return (
    <div className="dialog-body f-column absolute-center">{props.children}</div>
  );
};

const DialogBox = ({ children }: { children: ReactNode }) => {
  return <div className="dialog-box">{children}</div>;
};

const DialogFooter = ({
  onClickSuccess,
  onClickCancel,
  titleSuccess,
  titleCancel,
}: CommonDialogFooterProps) => {
  return (
    <div className="dialog-footer">
      {onClickCancel && (
        <button className="cancel-dialog-button" onClick={onClickCancel}>
          {titleCancel}
        </button>
      )}
      {onClickSuccess && (
        <button className="success-dialog-button" onClick={onClickSuccess}>
          {titleSuccess}
        </button>
      )}
    </div>
  );
};

export const CommonDialog = (props: CommonDialogProps) => {
  const { message, title }: IDialogData = useSelector(
    commonSelector.getDialogData
  )[props?.dialogId] ?? { message: "", title: "" };

  const dispatch = useDispatch();
  const onClose = useCallback(() => {
    dispatch(commonActions.closeLayer(props?.dialogId));
  }, [dispatch, props?.dialogId]);

  return (
    <Layer layerId={props.dialogId}>
      <div className="dialog-wrapper">
        <DialogBox>
          <DialogHeader title={title ?? ""} />
          <DialogBody>
            <h4>{message}</h4>
          </DialogBody>
          <DialogFooter
            onClickCancel={onClose}
            titleCancel="Close"
            titleSuccess=""
          />
        </DialogBox>
      </div>
    </Layer>
  );
};
