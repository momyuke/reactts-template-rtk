import { MouseEvent, useCallback } from "react";
import { IProduct } from "../../../pages/home/product.domain";
import style from "./card-product.module.css";

interface ICardProductProps {
  product: IProduct;
  onClickCart?: (id: number) => void;
}
export const CardProduct = ({ product, onClickCart }: ICardProductProps) => {
  const onClick = useCallback((e: MouseEvent<HTMLButtonElement>) => {
    if (onClickCart) onClickCart(Number(e.currentTarget.id));
  }, []);
  return (
    <div className={style.cardProduct}>
      <img
        src="https://plus.unsplash.com/premium_photo-1677541367608-7283ec1b3a2b?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
        alt="Image Product"
        width={200}
        height={200}
      />
      <h3 className={`no-margin`}>{product.name}</h3>
      <p className={`no-margin`}>Stock Left: {product.stock}</p>
      <h4 className={`no-margin`}>Rp. {product.price}</h4>
      <button id={`${product.id}`} onClick={onClick}>
        + To Cart
      </button>
      <p></p>
    </div>
  );
};
