import axios from "axios";

import { CommonDialogId } from "./common/common.domain";
import { commonActions } from "./common/common.slice";
import { AppDispatch } from "./slice.domain";
import { dispatch } from "./slice.index";

export const apiAxios = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
});

apiAxios.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("accessToken");

    if (token != null) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export const interceptorResponseAxios = (
  dispatch: AppDispatch,
  axiosIndex = apiAxios
) => {
  axiosIndex.interceptors.response.use(
    (response) => {
      return response;
    },
    (err) => {
      console.log(err);

      dispatch(
        commonActions.updateData({
          message: err?.response?.data?.message ?? "There is an error",
          key: CommonDialogId.GENERAL_ERROR,
          title: "Ooops",
        })
      );

      dispatch(commonActions.openLayer(CommonDialogId.GENERAL_ERROR));
      return Promise.reject(err);
    }
  );
};

interceptorResponseAxios(dispatch);
