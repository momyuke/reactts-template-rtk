import { apiAxios } from "../../index.api";
import { ILoginRequest } from "./auth.domain";

const AUTH = "/user";

const AUTH_API = {
  LOGIN: AUTH + "/login",
};

export const loginApi = (loginRequest: ILoginRequest) => {
  return apiAxios.post(AUTH_API.LOGIN, loginRequest);
};
