import { useCallback, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { CommonDialogId } from "../../common/common.domain";
import { Loading } from "../../common/component/loading";
import { useAppDispatch } from "../../common/hooks/common.hooks";
import { login } from "./auth.thunk";
import style from "./login.module.css";

const Login = () => {
  const dispatch = useAppDispatch();
  const usernameRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);
  const navigate = useNavigate();
  const onLogin = useCallback(() => {
    dispatch(
      login(
        {
          username: usernameRef.current?.value ?? "",
          password: passwordRef.current?.value ?? "",
        },
        navigate
      )
    );
  }, [dispatch, navigate]);

  return (
    <main className={`f-column absolute-center`}>
      <div className={`${style.loginSection} f-column`}>
        <h2 className="no-margin">Login Page</h2>

        <div className={`${style.inputSection} f-column`}>
          <label htmlFor="username">Username</label>
          <input
            ref={usernameRef}
            type="text"
            placeholder="yourusername"
            name="username"
          />
        </div>
        <div className={`${style.inputSection} f-column`}>
          <label htmlFor="password">Password</label>
          <input
            ref={passwordRef}
            type="password"
            placeholder="yourpassword"
            name="password"
          />
        </div>
        <button onClick={onLogin}>Login</button>
      </div>
      <Loading loadingId={CommonDialogId.LOGIN} />
    </main>
  );
};

export default Login;
