import { NavigateFunction } from "react-router-dom";
import { Dispatch } from "redux";
import { CommonDialogId, GeneralResponse } from "../../common/common.domain";
import { commonActions } from "../../common/common.slice";
import { loginApi } from "./auth.api";
import { ILoginRequest } from "./auth.domain";

export const login = (
  loginRequest: ILoginRequest,
  navigate: NavigateFunction
) => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(commonActions.openLayer(CommonDialogId.LOGIN));
      const response = await loginApi(loginRequest);
      const { data }: GeneralResponse<unknown> = response.data;
      navigate("/home");
      console.log(data);
    } catch (e) {
      /* empty */
    } finally {
      dispatch(commonActions.closeLayer(CommonDialogId.LOGIN));
    }
  };
};
