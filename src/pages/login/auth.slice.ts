
import { CaseReducer, PayloadAction, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../slice.index";
import { IAuthInitialState, IViewAuthState } from "./auth.domain";

export const authSelector = {
  getView: (state: RootState): IViewAuthState => state.auth.view,
};

const initialState: IAuthInitialState = {
  view: {
    isLogin: false,
    userId: "",
  },
};

const updateView: CaseReducer<
  IAuthInitialState,
  PayloadAction<IAuthInitialState>
> = (state, { payload }) => {
  console.log(state);
  return {
    ...state,
    ...payload,
  };
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    updateView,
  },
});

export const authActions = authSlice.actions;
export default authSlice;

