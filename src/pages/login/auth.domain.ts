export interface IAuthInitialState {
  view: IViewAuthState;
}

export interface IViewAuthState {
  isLogin: boolean;
  userId: string;
}

export interface ILoginRequest {
  username: string;
  password: string;
}
