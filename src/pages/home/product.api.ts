import { apiAxios } from "../../index.api";

const PRODUCT = "/product";

export const getProductsApi = () => {
  return apiAxios.get(PRODUCT);
};