import { CaseReducer, PayloadAction, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../slice.index";
import { IProductInitialState, IViewProductState } from "./product.domain";

export const productSelector = {
  getView: (state: RootState): IViewProductState => state.product.view,
};

const initialState: IProductInitialState = {
  view: {
    products: [],
    selectedProduct: {
      id: 0,
      createdAt: "",
      name: "",
      price: 0,
      stock: 0,
      updatedAt: "",
    },
  },
};

const updateView: CaseReducer<
  IProductInitialState,
  PayloadAction<IProductInitialState>
> = (state, { payload }) => {
  return {
    ...state,
    ...payload,
  };
};

const selectProduct: CaseReducer<
  IProductInitialState,
  PayloadAction<number>
> = (state, { payload }) => {
  const selectedProduct = state.view?.products?.find((e) => e.id === payload);
  return {
    ...state,
    view: {
      ...state.view,
      selectedProduct,
    },
  };
};

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    updateView,
    selectProduct,
  },
});

export const productActions = productSlice.actions;
export default productSlice;
