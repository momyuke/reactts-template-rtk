export interface IProductInitialState {
  view: IViewProductState;
}

export interface IViewProductState {
  products?: IProduct[];
  selectedProduct?: IProduct;
}

export interface IProduct {
  id: number;
  name: string;
  stock: number;
  price: number;
  createdAt: string;
  updatedAt: string;
}

export const productDummyImage =
  "https://plus.unsplash.com/premium_photo-1677541367608-7283ec1b3a2b?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D";
