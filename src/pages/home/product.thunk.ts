import { Dispatch } from "redux";
import { GeneralResponse } from "../../common/common.domain";
import { commonActions } from "../../common/common.slice";
import { getProductsApi } from "./product.api";
import { IProduct } from "./product.domain";
import { productActions } from "./product.slice";

export const getProducts = () => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(commonActions.openLayer("PRODUCT"));
      const response = await getProductsApi();
      const { data }: GeneralResponse<IProduct[]> = response.data;
      dispatch(productActions.updateView({ view: { products: data ?? [] } }));
    } catch (e) {
    } finally {
      dispatch(commonActions.closeLayer("PRODUCT"));
    }
  };
};
