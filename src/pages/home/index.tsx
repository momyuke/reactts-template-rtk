import { useCallback, useEffect } from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../../common/hooks/common.hooks";
import style from "./home.module.css";
import { productActions, productSelector } from "./product.slice";
import { getProducts } from "./product.thunk";
import { CardProduct } from "@/common/component/cardproduct";

const Home = () => {
  const dispatch = useAppDispatch();
  const { products } = useSelector(productSelector.getView);

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const onAddCart = useCallback(
    (id: number) => {
      dispatch(productActions.selectProduct(id));
    },
    [dispatch]
  );

  return (
    <main className={`f-row ${style.productSection}`}>
      {(products?.length ?? 0) > 0 &&
        products?.map((product) => {
          return <CardProduct onClickCart={onAddCart} product={product} />;
        })}

      {(products?.length ?? 0) <= 0 && (
        <>
          <h2>No Data</h2>
        </>
      )}
    </main>
  );
};
export default Home;
