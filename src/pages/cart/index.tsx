import { IProduct, productDummyImage } from "../home/product.domain";
import style from "./cart.module.css";

const Cart = () => {
  const productDummy: IProduct = {
    id: 5,
    createdAt: "",
    updatedAt: "",
    name: "Monitor LED VIewsonic",
    price: 12000,
    stock: 2,
  };
  return (
    <main className={`${style.cartWrapper} f-row`}>
      <section className={`${style.productsWrapper} f-column`}>
        <CardCart amount={2} product={productDummy} />
        <CardCart amount={2} product={productDummy} />
        <CardCart amount={2} product={productDummy} />
      </section>
      <section className={`${style.calculateWrapper} f-column`}>
        <h2 className="no-margin">Total</h2>
        <h2 className="no-margin">Rp. 12.000.000</h2>
        <button>Checkout</button>
      </section>
    </main>
  );
};

interface ICardCardProps {
  product: IProduct;
  amount: number;
}
const CardCart = ({ product, amount }: ICardCardProps) => {
  return (
    <div className={`${style.cardCart} f-row`}>
      <aside className={`${style.left}`}>
        <img src={productDummyImage} alt="product" />
      </aside>
      <aside className={`${style.right} f-column`}>
        <p className="no-margin">Stock Left {product.stock}</p>
        <h2 className="no-margin">{product.name}</h2>
        <p className="no-margin">Rp. {product.price}</p>
        <h4 className="no-margin">Amount {amount}</h4>
      </aside>
      <aside className="f-row absolute-center">
        <button>Remove</button>
      </aside>
    </div>
  );
};

export default Cart;
