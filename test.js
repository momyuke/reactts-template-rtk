const mantap = {
  Biaya: "Test",
  "Check Limit": "Test",
  Peeking: "Test",
  Privacy: "Test",
  "Beli Dan Bayar": "Test",
};

const notAllowedTerms = ["Biaya", "Check Limit", "Privacy"];

const allowedTerms = Object.keys(mantap)
  .map((key) => {
    if (!notAllowedTerms.includes(key)) return mantap[key];
    return null;
  })
  .filter((e) => e !== null);

console.log(allowedTerms);
